# Gitlab Sample C++ Project

[![pipeline status](https://gitlab.com/Xterat/sample-gitlabci-cpp-project/badges/master/pipeline.svg)](https://gitlab.com/Xterat/sample-gitlabci-cpp-project/commits/master)
[![coverage report](https://gitlab.com/Xterat/sample-gitlabci-cpp-project/badges/master/coverage.svg)](https://gitlab.com/Xterat/sample-gitlabci-cpp-project/commits/master)

## Documentation

Simply clone this repository in your Gitlab instance, and make sure to have a Gitlab runner defined (preferably with a docker-based executor) and you'll see your gitlab ci builds working!
